---
weight: 1
title: "L'autogestion de sa santé naturelle - Avant Propos"
date: 2020-05-05T21:40:32+08:00
lastmod: 2020-03-06T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["santé", "autogéstion", "transmition"]
categories: ["Autogestion Santé Naturelle"]
featuredImage: "/images/blog/rsz_article1.jpg"

lightgallery: true

toc:
  auto: false
---



> ***Etymologie*** :  Du grec « autos », soi-même et du latin « gestio » ; donc gérer par soi- même, par un groupe ou par l’ensemble d’un pays,
> une situation quelconque permettant une réappropriation de notre vie
> individuelle ou commune, sans l’intermédiaire d’un pouvoir pyramidale
> dirigeant nos actions.

En ce qui concerne « l’autogestion de sa Santé Naturelle », cela veut dire que chacun(e)
gérons par nous-mêmes notre santé en connaissance de cause, en prenant compte de la
globalité de notre état d’être, sans qu’une hiérarchie médicale nous impose des méthodes
antinaturelles, pour soit disant conserver (prévention) ou retrouver notre santé dans les cas
non irréversibles. Méthodes antinaturelle qui installent une santé artificielle, par
l’intermédiaire de médicaments chimiques.

Depuis notre naissance nous apprenons à développer des moyens pour augmenter notre
autonomie. Juste après notre naissance, comme tous les animaux, nous trouvons par nous
même le téton du sein de notre mère pour nous nourrir. Nous faisons notre premier acte
d’autonomie en tant qu’être humain.

Par la suite nous apprenons à marcher, à faire du vélo, à nager, à conduire, à nous servir de
nos mains, de notre cerveau…Nous développons nos autonomies pour nous diriger dans
notre existence.

Ces autonomies peuvent servir à la société sous forme d’entraide, de partage et de
solidarité. Mais l’acte d’autonomie individuel qu’est l’autogestion de sa santé naturelle et qui
peut servir également la société est complètement occulté par toutes les autorités
gouvernementales. Car dans notre société capitaliste une personne malade est obligée de
ralentir ou d’arrêter sa contribution à la société par son travail.

Face à cette occultation volontaire des autorités gouvernementales, il n’y a que nous-
mêmes qui pouvons être les propres acteurs de notre santé naturelle.
C’est-à-dire que chaque individu après avoir acquis cette autogestion puisse, comme il le
désir, et en connaissance de cause, conserver, préserver, améliorer, retrouver, régénérer SA
SANTE NATURELLE.

Ce qui nous permet, en sachant ce qu’il faut faire au sujet de notre santé, de ne plus être
entravés longuement par la maladie et par ce fait de pouvoir être disponible pour une
autogestion sociale et sociétale.
« *Nous pouvons toujours changer notre destin, c’est là notre destin. Mais sachons que
nous sommes tributaire de notre destinée, car elle ne peut-être évitée.* » (Vénérable
Aryadeva : 17 février 1917- 8 juin 1970)

Notre système sociétal, basé sur le profit capitaliste par l’exploitation des individus,
entrave l’épanouissement de l’autonomie individuelle et collective. Comme nous pouvons le
constater aucune mesure n’est prise par l’éducation nationale pour permettre, aux enfants et
aux adolescents de recevoir cet enseignement apportant les connaissances pour autogérer
leur santé naturelle. Pour qu’une fois adultes, ils puissent être les acteurs de leur
VERITABLE SANTE, ou bien choisir de ne pas s’en occuper et de se remettre entre les mains des différentes médecines (allopathique, homéopathique, naturelle…) pour maintenir leur **santé artificielle**.



## Autogérer sa Santé Naturelle est un acte.
![enter image description here](https://images.pexels.com/photos/60582/newton-s-cradle-balls-sphere-action-60582.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=1260)

Autogérer sa Santé Naturelle est un acte d’autonomie qui devrait être transmis dans toutes
les écoles primaires dès le CM1, dans tous les collèges et tous les lycées.

Autogérer sa Santé Naturelle est un acte qui nous apprend à nous connaître, physiquement,
psychologiquement et de savoir où nous en sommes avec notre état d’esprit.

Autogérer sa Santé Naturelle est un acte qui ne permet plus à quiconque s’appuyant sur un
titre de docteur de gérer notre santé en la rendant artificielle.

Autogérer sa Santé Naturelle est un acte qui fait barrière à toutes les publicités
médicamenteuses, enrichissant les multinationales pharmaceutiques par notre ignorance,
évitant ainsi à notre organisme et notre mental de devenir des poubelles à poisons.

Autogérer sa Santé Naturelle est un acte qui nous fait prendre conscience que
physiquement nous ne sommes qu’un ensemble de cellules (un monde cellulaire en symbiose
avec l’ensemble des micro-organismes [bactéries, champignons, virus …] qui vivent en
équilibre à la surface et dans le corps humain. Ce qui est nommé le microbiote).
Cellules douées d’une intelligence, qui savent choisir les substances nutritives qui sont
nécessaires à leur entretient et à leur renouvellement, puis évacuer les substances nocives, si
nous ne polluons pas leur milieu et les aidons par notre façon de vivre.

Autogérer sa Santé Naturelle est un acte qui par solidarité, nous devenons des
transmetteuses, des transmetteurs, envers la société, pour toutes celles et pour tous ceux qui
veulent à leur tour savoir Autogérer leur Santé Naturelle et la **transmettre**.

Autogérer sa Santé Naturelle est un acte conscient, d’une action politique-économique-
écologique-sociologique (ces quatre actes étant interdépendants les uns des autres, ne pouvant
pas être séparés, agissant les uns sur les autres).

La Santé occupe une place importante dans notre existence, elle participe pleinement à
l’action politique-économique-écologique-sociologique. Pour cela il est absolument essentiel
que nous ne soyons pas tributaire des trusts labo-pharmacologiques et de la médecine
allopathique, dont une très grande partie de la recherche est à la solde de ces labos, qui
veulent diriger notre santé. La médecine allopathique doit avoir seulement sa place dans les
cas d’extrême urgence, pour lesquels les pratiques naturelles peuvent l’accompagner afin
d’aider l’organisme à supporter les actions thérapeutiques brutales pour celui-ci et l’aider
également à retrouver une remonté de vitalité.

Il est également fondamental que notre nourriture ne soit pas infectée, empoisonnée par la
main mise de l’agrochimie-industrie sur celle-ci. Que par des actions de solidarités aux petites
et moyennes fermes à échelle humaine, l’agriculture paysanne puisse se développer au
maximum, apportant dans nos assiettes de quoi soutenir notre Santé Naturelle, et, remplacer à
la longue la mortifère agrochimique-industrielle.

## Par rapport à la santé, nous avons trois options.

 Laisser gérer notre santé par le corps médical officiel et nous nous trouvons avec une
**santé artificielle.**
- Laisser gérer notre santé par une des médecines douces (homéopathie, médecine dite
naturelle et alternative….). Mais nous sommes toujours tributaires de quelqu’un(e) et de
techniques passant de l’une à l’autre (granules homéopathiques, oligoéléments, acupuncture,
ostéopathie, huiles essentielles, probiotiques…..). Sans pour cela changer notre manière de
vivre. Restant esclave de cabinets médicaux alternatifs. Et notre santé reste toujours
**artificielle**
- Soit nous décidons de devenir **acteur de notre santé et autogérons celle-ci en
connaissance de cause.**
C’est-à-dire, que nous apprenons à vivre en harmonie avec le monde cellulaire qui nous
constitue. Que nous comprenions que les cellules sont intelligentes, savent gérer ce dont elles
ont véritablement besoin et éjecter les déchets dû à ce que nous nommons le métabolisme
cellulaire ; faire face aux éléments extérieurs nuisibles qui veulent s’installer dans leur milieu.
Ainsi pouvoir régir convenablement leur existence et leur reproduction.

Bien entendu, en leur apportant les éléments naturels dont elles ont véritablement besoin.

> « Le corps humain est un lieu d’échanges perpétuels ; un complexe de
> cellules vivantes interdépendantes dans un océan intérieur animé par
> des courants, une pulsion et une respiration fondamentale. Un tour
> indissociable…………. Le corps humain, autogéré par une sagesse
> naturelle, est un récepteur, un transformateur et un émetteur
> d’énergie qui ne peut être compris que dans ses interactions avec le
> milieu». 
> (Docteur Georges Jaeger)


## Comment savoir autogérer sa Santé Naturelle ?
![enter image description here](https://images.pexels.com/photos/1046896/pexels-photo-1046896.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=1260)

En s’appropriant l’enseignement de la Naturopathie contemporaine authentique, pour
l’inclure dans sa vie quotidienne. Par laquelle nous apprenons le fonctionnement de notre
organisme, non dans l’esprit de le dominer comme le fait la médecine allopathique, mais
dans celui de collaborer avec lui pour que l’ensemble qui le compose, nos cellules et le
monde bactérien qui l’habite (les fameux microbiotes anciennement nommés flore
microbienne), puissent vivre en harmonie dans leur milieu d’échange d’échanges perpétuels.
Milieu, pour les cellules qui nous constituent physiquement, d’échanges perpétuels, animé
par un flux et reflux de l’océan intérieur, qui les baigne, influé par une respiration
fondamentale, des courants et une pulsation. C’est-à-dire la VIE.
 
### 1.  D’où vient le terme naturopathie ? Et pourquoi contemporaine ?

Bien que la pratique naturopathique soit vielle comme le monde. L’initiateur du mot
naturopathie fut John H. Scheel (Autrichien émigré aux Etats-Unis) en 1895, en réunissant
les mots « nature » et « path » (qui veut dire chemin, sentier, voie), signifiant «le sentier de la
nature ». Pour John H. Scheel, la Santé ne peut qu’avoir lieu du respect de la nature.

En 1902 Benedict, s’appropriant le mot naturopathie, fonda à New-York la première école
de naturopathie.

En 1935 le biologiste Pierre-Valentin Marchesseau (1911-1994), se basant sur les
principes fondamentaux en accord avec les lois naturelles du milieu spécifique à l’Etre
Humain, après avoir réalisé une synthèse des différentes méthodes naturelles de santé, créa la
« Naturopathie Contemporaine ». Définissant le mot naturopathie par la composition du latin
« natura » et du grec « pathos » signifiant la maladie étudiée en fonction de la Nature. Pour
P.V Marchesseau, le milieu naturel de l’Etre Humain fait la Santé si celui-ci est dans des
éléments normaux, spécifiques, mais si le milieu naturelle comporte des éléments anormaux
la Santé s’amenuise petit à petit jusqu’à l’apparition de la maladie.

> «La maladie, c’est un drame en 2 actes, dont le premier se joue dans
> le morne silence de nos tissus toutes lumières éteintes. Quand la
> douleur ou d’autres gênes arrivent, on est presque toujours au second
> acte» (Dr-Chirurgien Leriche 1879-1955).»

Nous voyons que malgré la différence de définition du mot naturopathie, John H.Scheel et
P.V Marchesseau s’accordent sur le maintien ou la suppression de la Santé en rapport avec le
milieu naturel de l’Etre Humain.

### 2) Pourquoi authentique ?

L’essence de la naturopathie n’est pas une médecine, ni douce, ni une médecine
alternative. Cette démarche est celle de la naturothérapie, comme son suffixe le montre elle
propose des thérapies.
Dans l’esprit de la naturopathie authentique, tous les moyens pratiques (qui sont au nombre
de 10, placés dans un triptyque) que nous verrons ensemble, se trouvent dans notre vie
quotidienne. Il suffit de les replacer à leur bonne place et de les pratiquer dans notre vie
courante. Ces pratiques une foi acquises et imbriquées dans la vie quotidienne, la
Naturopthie , n’existe plus. C’est un Art de Vivre.
De plus selon Pierre-Valentin Marchesseau «Ni guérisseur, ni médecin, le Naturopathe est
avant tout un hygiéniste ou un éducateur et mieux encore, un professeur de Santé.»

C’est-à-dire que le naturopathe est un transmetteur de cet enseignement pour tous(tes)
ceux et celles qui veulent se l’accaparer et l’appliquer dans leur quotidien comme Art de
Vivre.

## Santé d’accord, mais santé naturelle.
Dans notre société nous pouvons constater que la santé artificielle a pris le dessus sur la
SANTE NATURELLE.

La santé artificielle est maintenue par des substances chimiques et des techniques
artificielles (simulateur cardiaque -pacemaker-, cœur artificiel….) qui nous font croire qu’ils
sont capables de protéger notre santé ou de la restaurer tout en considérant que nous pouvons
continuer notre existence contre nature, sans rien changer à nos habitudes de vie qui sont
contraires aux lois et conditions du milieu spécifique à l’Etre Humain.

La Santé Naturelle consiste non pas dans la « magie » des principes chimiques actifs, ni
dans les techniques artificielles médicales, mais dans un accord parfait de l’Etre Humain et
de son milieu spécifique.

## Sur quelle base aura lieu cette transmission ?
![enter image description here](https://images.pexels.com/photos/301614/pexels-photo-301614.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=1260)

Sur l’enseignement de la Naturopathie Contemporaine transmise par P.V Marchesseau,
dont l’étude essentielle porte sur les lois de la Santé.
Les bases de cette étude sont :
- ***Le Vitalisme***, base philosophique qui considère l’Energie Vitale comme principe
indispensable à la santé, s’appuyant sur « l’intelligence cellulaire ».
- ***L’Humorisme***, base scientifique s’appuyant sur la fluidité des liquides humoraux (sang,
lymphe, sérums extra-intracellulaire).
- ***Le Causalisme***, recherche des éléments provoquant le dérèglement organique et la chute de
la santé. En partant du fait, qu’en Naturopathie, toute maladie est UNE et résulte de
l’ENCRASSEMENT DES LIQUIDES HUMORAUX.
- ***L’Hygiénisme***, aider l’organisme à conserver ou à retrouver la Santé Naturelle, en l’aidant
à épurer les liquides humoraux de tous les déchets métaboliques ou autres, pour maintenir ou
retrouver dans celui-ci une vie cellulaire normale.
Pratique réalisée par l’intermédiaire de 10 techniques naturelles appliquées dans 3 cures :
    - *Désintoxication* qui permet l’élimination des toxines empoisonnant les liquides
    humoraux.
    - *Revitalisation*, afin de regagner la vitalité perdue.
    - *Stabilisation* pour que chacun puisse mettre en place le bon équilibre des conditions de
    vie.

- ***Le Globalisme***, à l’inverse de la médecine allopathique, médecine de diagnostics et de
traitements fragmentaires, la Naturopathie Contemporaine s’appuie, non sur la « magie » des
principes chimiques actifs, mais dans un accord parfait de l’Etre Humain et de son milieu
spécifique en s’appuyant par l’étude de la Sagesse du Corps en prenant l’Etre Humain dans
son intégralité (biologique-physiologique, mental-émotionnel, état d’esprit).

*Jacques Lefort*

***A suivre***

