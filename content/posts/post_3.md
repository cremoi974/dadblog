---
weight: 1
title: "Base scientifique en naturopathie contemporaine - Partie 2/3"
date: 2020-07-11T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["santé", "science", "naturopathie"]
categories: ["Base scientifique en naturopathie contemporaine"]
featuredImage: "/images/blog/rsz_1pexels-photo-132477.jpg"

lightgallery: true

toc:
  auto: false
---


# QU’EST-CE QUE NOTRE ORGANISME ?

  

Ce n’est qu’un ensemble de cellules qui vont constituer des tissus, qui eux-mêmes constituent des organes et qui par l’assemblage des organes vont établir nos systèmes physiologiques et ainsi par leur fonctionnement constituent notre organisme.
Pour un être humain de 70kg et de 1m70, il est évalué à 3.10puissance13 le nombre de cellules, soit 30000 milliards réparties en 300 sortes.

## La Cellule
Pour bien comprendre l’importance de savoir autogérer sa Santé Naturelle, et, lorsqu’elle devient défaillante comment la retrouver et ne pas attendre le non retour. Un petit tour dans ce monde de vie qui constitue nos cellules.

### 1) Propriétés de la cellule
De nouveau à l’intérieur de nos cellules **un monde complexe vit, qui manifeste des propriétés vitales** assurant l’unité interne de notre organisme.
Ces propriétés vitales et fondamentales sont :

 - **Métabolisme et croissance.** Par l’utilisation d’une partie des matières résorbées nos cellules élaborent leur propre substance.
 - **Reproduction.** Par ses manifestations la Vie cherche toujours à se développer. Ainsi nos cellules se divisent pour donner naissance à
 deux autres cellules.
- **Sensibilité et mouvement.** En évaluant les impressions perçues des milieux ambiants externe et interne, nos cellules y répondent par des mouvements internes et externes.
- 

### 2) Structure de la cellule

Cette structure vivante de nos cellules est composée de **cytoplasme** et d’un ou plusieurs **noyaux** (cependant les globules rouges ne comportent pas de noyaux) et d’une **membrane**.

#### a) Le Cytoplasme.

C’est la substance vivante de nos cellules. Il est semi-liquide composé de trois quart **d’eau** (que nous nommons en biologie naturopathique **le sérum intracellulaire**  une des bases de **l’Humorisme**, l’autre étant le **sérum extracellulaire** que nous verrons plus tard). Du point de vue chimique les éléments les plus abondants sont l’oxygène, le carbone, l’hydrogène et l’azote, qui constituent environ 99% de tous les atomes du cytoplasme.

Le dernier quart de la composition du cytoplasme comprend des protéines, des lipides ou corps gras, des glucides et des sels minéraux (dont le phosphate et le potassium sont les plus abondants.)

Dans le cytoplasme se tient une série de structures nommées **organites cellulaires**. Elles ont chacune une fonction hautement spécialisé.
- Le Réticulum endoplasmique : Système de cavité qui se dilate plus ou moins, composé de petits corpuscules, nommés, ribosomes qui synthétisent les protéines. A l’intérieur de ces cavités circule un liquide qui permet un transport rapide et continu de matières solubles dans le cytoplasme. Donc deux fonctions, synthèse des protéines et transport intracellulaire.
- L’Appareil de Golgi : C’est l’organisme cellulaire chargé de la production des sécrétions, remplissant une fonction glandulaire.
- Les Mitochondries : Ils sont les supports des ferments respiratoires et les centres énergétiques du milieu cellulaire. Procurant en majorité l’énergie qui permet aux cellules d’accomplir les fonctions primordiales à leur vie et à celle de notre organisme.
- Les Ribosomes : Nous avons vu les ribosomes qui forment le réticulum endoplasmique et qui servent à synthétiser les protéines qui vont être **expulsées hors des cellules dans le milieu extracellulaire**. Mais des ribosomes se trouvent également libres dans le cytoplasme. Ceux-ci produisent des protéines **pour l’usage propre des cellules**.
Les ribosomes sont « **les centrales d’énergie** » des cellules.
- Les Lysosomes : A l’apparence très variable, ils changent de taille et de forme selon leur activité. Ils contiennent différents sortes de ferments (enzymes) qui dégradent les déchets cellulaires à l’intérieur de la cellule, qui détruisent des éléments venant de l’extérieur de la cellule et inutile à celle-ci (les globules blancs contiennent un grand nombre de lysosomes, après avoir absorbé les bactéries inutiles à notre organisme, les détruisent et les digèrent à travers ceux-ci). C’est le système digestif de nos cellules.

  

#### b) Le Noyau.

Il occupe le centre de la cellule. Il est entouré d’une double membrane (membrane nucléaire) comportant des orifices nucléaires, lui permettant un échange entre le contenu nucléaire et le cytoplasme. Il est également en communication avec réticulum endoplasmatique.

Dans le noyau ce trouve un organite, le nucléole qui est le siège des ARN (acides ribonucléiques), tandis que dans la substance nucléaire du noyau se situe ADN (acides désoxyribonucléiques) support de l’information génétique.

  

#### c) La Membrane cellulaire. 

En biologie naturopathique, la membrane cellulaire a une très grande importance. C’est elle qui va faire le lien entre l’environnement de la cellule (sérum extracellulaire, comme nous le nommons en Naturopathie contemporaine) et l’intérieur de la cellule, dans laquelle se manifeste également la Vie.
Elle délimite la cellule. Elle est formée de trois couches. Une couche interne qui est en rapport avec le cytoplasme. Une couche externe qui est en rapport avec le milieu extracellulaire.
Ces deux couches sont composées de phospholipides qui ont la forme d’une sucette à deux bâtonnets. Ces bâtonnets forment la troisième couche, avec un espace entre ceux des phospholipides formant la membrane interne et ceux formant la membrane externe.

Les têtes des phospholipides sont polarisées tandis que les bâtonnets sont non polarisés, formant un isolant en vue d’empêcher la cellule d’être envahie par toutes les molécules du milieu extracellulaire.
Comme la plupart des aliments, dont la cellule a besoin, sont polarisés, par cette barrière non polarisée, la cellule ne pourrait pas se nourrir, ni éliminer ses déchets qui sont également polarisés.

Pour faire face à ceci des **protéines – Protéines Membranaires Intrinsèques (PMI)**- traversent les trois couches, permettant aux nutriments, aux autres informations (comme celle de la lumière, des sons, des champs vibratoires et des fréquences radios), nécessaires à la cellule et à ses déchets qui sont polarisés de traverser la membrane cellulaire.
Les PMI sont divisées en deux classes : protéines réceptrices et protéines effectrices.
**- Les PMI réceptrices**  (sur la représentation schématique) : sont les «organes» sensoriels de la cellule. Elles réagissent à des signaux spécifiques aussi bien du milieu extracellulaire, qu’intracellulaire.
Pour surveiller le milieu extracellulaire et intracellulaire, certaines Protéines Membranaires Intrinsèques réceptrices sont tournées vers l’extérieur des cellules, tandis que d’autres sont tournées vers l’intérieur des cellules.
Les deux positions des PMI réceptrices permettent aux cellules de lire les différents champs énergétiques aussi bien ceux des molécules physiques que ceux de notre état psychologique. C’est-à-dire que le comportement biologique de nos cellules peuvent être contrôlés aussi bien que par des molécules que par des impacts invisibles, comme la pensée.
Mais le comportement de nos cellules ne peut être uniquement influencé par les PMI réceptrices qui servent exclusivement à détecter les signaux des milieux intra et extracellulaire.

- **Les PMI effectrices.** (Sur la représentation schématique) : sont les «organes» de contrôle et de transport (protéines effectrices canal), faisant passer des molécules et des informations du milieu extracellulaire vers le milieu intracellulaire et vice et versa.
Les protéines effectrices réagissant aux signaux de l’environnement, contrôlent ainsi la «lecture» des gènes pour que les protéines épuisées puissent être remplacées par de nouvelles protéines.
Pour que nos cellules puissent réagir de façon appropriée et durable aux stimuli, les PMI effectrices prennent le relais.

> Ainsi les PMI réceptrices-effectrices forment le couple stimuli-réaction. Nous pouvons faire une comparaison avec notre système nerveux. Lorsque nous touchons avec la main quelque chose de piquant (exemple des orties), les nerfs sensoriels de la main captent le signal qui le transmet immédiatement aux nerfs moteurs qui nous  fait retirer la main.
> De même lorsque nous entendons des paroles qui nous déplaisent ou qui nous rendent joyeux, nous réagissons à celles-ci.

![alt text](/images/blog/yolo.jpg "Schema")