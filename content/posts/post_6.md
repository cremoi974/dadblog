---
weight: 1
title: "La sante naturelle et la maladie en naturopathie contemporaine: Deuxième partie"
date: 2020-08-26T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["maladie", "étude", "naturopathie"]
categories: ["La sante naturelle et la maladie en naturopathie contemporaine"]
featuredImage: "/images/blog/post6-banner.jpg"

lightgallery: true

toc:
  auto: false
---

## L’IMPORTANCE DE SAVOIR AUTOGERER SA SANTE NATURELLE

>> *« Le but de cette connaissance est, non pas de satisfaire notre curiosité, mais de nous reconstruire nous-mêmes, et de modifier notre milieu dans un sens qui nous soit favorable »*
Alexis Carrel

L’on nous inculque que nous sommes indépendants de notre environnement, de la Nature,
grâce à notre soit disant intelligence, par l’invention d’outils, de machines…..Là est notre grosse erreur.
Nous sommes esclaves, tributaires du milieu que nous avons créé. Au lieu d’être en accord, en harmonie avec un milieu spécifique à l’Etre Humain et libre dans une société à Echelle
Humaine.
Depuis plus de 2500 ans l’adage, inscrit au frontispice du temple de Delphes, que l’éducation
nationale a laissé ensevelis sous les ruines de la civilisation de la « Grèce antique », nous indique le chemin :***« Connais-toi, toi-même et tu connaitras l’univers et les dieux »***.
L’importance dans cet adage, c’est la ***Connaissance de soi-même***.
Mais comment se connaître soi-même ?
* Par une connaissance du fonctionnement de son corps, qui n’est pas compliquée dans l’essentiel.
* En sachant où nous en sommes psychologiquement et émotionnellement.
* En scrutant notre état d’esprit.

Par la liaison de ces trois aspects, qui sont interdépendants, et en s’accaparant des données de
la Naturopathie Contemporaine, prendre en mains sa santé pour en devenir la principale actrice, le principal acteur, pour ne plus être dépendant(e) d’une médecine aux ordres de « BigPharma ».

###  **I) Santé artificielle, Santé naturelle.**

* Santé artificielle entretenue et maintenue par des médicaments tout au long d’une vie humaine, sous la domination de la médecine.
* Santé naturelle entretenue et maintenue par la connaissance, sous l’égide de chaque individu, du bon fonctionnement de notre organisme, tributaire d’une vie saine de nos cellules dans leur milieu aquatique.
Milieu adéquate qui est le résultat d’un bon apport d’éléments matériels (solides, liquides, gazeux) et psychiques (respect, liberté, solidarité) spécifiques à l’Etre Humain.
Et par l’intermédiaire de moyens pratiques naturels qui sont tous les jours dans notre vie quotidienne.

### **II) Deux conceptions divergentes.**

* a) La médecine allopathique, par l’intermédiaire de ses biologistes, s’appuie sur l’espérance de **trouver des substances** (médicaments) capables de protéger l’Etre Humain ou de restaurer sa « Santé » en luttant contre des **agresseurs extérieurs**, jugés, de son point de vue, responsables.
S’appuyant sur le **traitement fragmentaire**, la médecine allopathique développe la théorie et la pratique du **remède spécifique à chaque maladie**, s’appuyant sur la « magie » des principes chimiques actifs.
* b) L’enseignement de la Naturopathie Contemporaine, pour être l’acteur(trice) de sa santé naturelle, à l’inverse de la médecine allopathique, s’appuie sur la connaissance desmoyens pour être en accord avec notre milieu spécifique.
C’est par esprit de synthèse que la démarche vers cette autogestion de sa santé naturelle peut se faire.
Contraire à l’esprit complètement analytique de la **médecine de diagnostique** et de **traitement fragmentaire** qui ne veut pas aller au-delà de l’expérimentation de laboratoire.
La Naturopathie Contemporaine pense *que le tout l’emporte sur le détail* et que pour chacun(e) de nous se comprenne, **mieux vaut s’examiner dans sa totalité** que dans ses parties qui ne sont jamais révélatrices des **ressources vitales disponibles.**
Cette démarche nous amène à connaître :
* Le milieu spécifique à l’Etre Humain.
* Les erreurs actuelles de vie dans notre milieu artificiel.
* Les moyens de retrouver notre milieu spécifique
* Dans le cas de retour impossible, comment **trouver des mesures compensatrices** par des méthodes de santé à travers des agents naturels.
> > Toute cette démarche se fait à travers l’étude de la sagesse du corps dont l’une des pierres angulaires est la vie harmonieuse de nos cellules dans leur milieu.

### **III) Quels sont ces milieux : milieu spécifique à l’Etre Humain et milieu cellulaire ?**
#### **1) Milieu spécifique à l’Etre Humain**

* C’est le milieu qui apporte à notre organisme les aliments solides, liquides et gazeux, sansêtre dénaturés, pour son équilibre et son bon fonctionnement.
* C’est le milieu dans lequel notre corps peut se développer harmonieusement.
* C’est le milieu dans lequel chaque individu peut se développer intellectuellement-manuellement comme il lui convient sans être stopper par un quelconque diplôme.
* C’est le milieu où sur le plan social, chaque individu trouve sa place selon ses capacitésintellectuelles et manuelles en y participant, sans être écrasé par une quelconque exploitationsur lui et enchaîné par une domination politique.
Milieu où l’équilibre psychique des individus a une très grande importance, où l’empathie, lacompassion, la solidarité sont des facteurs moteurs.

>>Pour synthétiser, c’est le milieu dans lequel nous devrions vivre afin de rester en SANTE physique et psychique, où la maladie ne devrait-être qu’un petit parcours.
Ceci peut paraître utopique. **L’Utopie c’est quelquechose qui n’est pas réalisée, mais pas forcément irréalisable. Tout dépend de la bonne volonté de chacun(e)**, en tenant compte dela possibilité. Là se place également la solidarité.

#### **2) Milieu cellulaire**
Nous avons vu précédemment que le milieu dans lequel baigne nos cellules était constitué :
* De liquide interstitiel ou lymphe interstitielle.
* De plasma sanguin.
* De liquide transcellulaire (nommé en Naturopathie sérum extracellulaire).
Certaine de nos cellules y circulent, comme les cellules sanguines, les lymphocytes. D’autres y stagnent c’est le cas des cellules qui forment nos tissus. Mais toutes **sont complètement dépendantes de l’état de ce milieu dans lequel elles vivent.**
Elles ont une interaction avec lui, en absorbant ce dont elles ont besoin et en rejetant ce quileur est nuisibles.
>> **Important :**
C’est donc dans cet élément liquide que nos cellules trouvent les éléments nécessaire à leurvie, pour leur maintient en santé jusqu’à leur mort ; pour leur reproduction, leur naissance,leur croissance et l’entretient de nos organes.
Ainsi la vitalité de notre organisme va dépendre dans son ensemble de la vitalité de nossystèmes physiologiques, qui eux vont dépendre de la vitalité des organes qui les constituent qui dépendront à leur tour des tissus qui les composent et en fin de compte **de la vitalité de nos cellules qui les constituent.**

#### **Conclusion**
Toute la vitalité de notre organisme, c’est-à-dire notre Santé, **est sous la dépendance de lavitalité de nos cellules, qui elles-mêmes sont tributaire du milieu dans lequel elles baignent.**

### **IV) Processus d’apport des éléments nécessaire à nos cellules et de l’élimination des déchets produits par celles-ci.**

Pour bien comprendre ce processus, observons ce simple schéma ci-dessous. Avec lequel il n’y a pas besoin de faire des années et des années d’études pour assimiler intellectuellement comment les éléments essentiels à nos cellules vont arriver jusqu’à elles. Et comment elles vont se débarrasser de leurs déchets. Ceci peut être très bien compris par des enfants qui se trouvent à l’école primaire en CM1 et CM2.

![yolo](/images/blog/post6_shema.png "Schema")

Par l’observation de ce schéma nous voyons que des organes bien spécifiques vont aider à introduire dans notre organisme, jusqu’à nos cellules les éléments spécifiques qui leur sont nécessaires.
Puis vont aider à l’évacuation des déchets dus au métabolisme cellulaire, une fois qu’elles se seront accaparées de ce qui leur est utile pour vivre, pour le bon fonctionnement de nos organes et la santé de notre organisme.
Comme nous le voyons sur le schéma, le trajet, des éléments nutritifs aux cellules, est : organes, sang, lymphe, sérum extracellulaire (Ce nous nommons en Naturopathie, le **milieu humoral cellulaire)**. Et pour les déchets le trajet inverse, sérum extracellulaire, lymphe, sang et organes qui les évacuent à l’extérieur de notre corps.
>>**Nota** :
Pour la distribution des éléments nutritifs, nous avons **5 organes**, comme il est montré sur leschéma : **Intestin , Foie + Vésicule biliaire, Poumons et Peau.**
Pour l’évacuation nous retrouvons ces **5 organes avec en plus les reins** qui eux n’apportent rien.

### **V) Ce Qu’il Faut Démontrer**

Nous savons que nos organes sont la résultante d’assemblages de nos cellules. Donc pour
que ceux-ci fonctionnent bien , i**l est naturellement normal et nécessaire que nos cellules doivent-être saines avec une bonne vitalité**, en recevant les éléments spécifiques à leur maintient en santé.
Nous verrons par la suite, par ce principe, la grande différence en ce qui concerne la **maladie**, et, le retour à la **santé,** entre la médecine et la naturopathie.