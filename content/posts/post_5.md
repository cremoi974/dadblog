---
weight: 1
title: "La sante naturelle et la maladie en naturopathie contemporaine: Prologue"
date: 2020-08-26T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["maladie", "étude", "naturopathie"]
categories: ["La sante naturelle et la maladie en naturopathie contemporaine"]
featuredImage: "/images/blog/post5.jpg"

lightgallery: true

toc:
  auto: false
---
Une étude réalisée par le Collège Impérial de Londres et l’OMS, donne les résultats du nombre de décès en 2016 dus aux Maladies Non Transmissibles : MNT (maladies cardiaques, accidents vasculaires cérébraux : AVC, cancers, diabète, et maladies pulmonaires chroniques).

Sur les 56,9 millions de décès dans le monde, **40,5 millions étaient dus à des maladies non transmissibles** (maladies que l’on surnomme souvent comme maladies de civilisation : cela veut tout dire……), **soit près de 111.000 morts par jour**, loin devant les autres causes de mortalité par la maladie, comme les maladies infectieuses.

##   I) Répartitions des décès selon l’âge.

Sur les 40,5 millions de décès dus aux maladies non transmissibles,

- 1,7 million de morts chez les moins de 30 ans (soit en moyenne environ 4658 par jour)

- 15,2 millions de morts chez les 30 à 69 ans (soit en moyenne environ 41644 par jour)

- 23,6 millions de morts chez les 70 ans et plus (soit en moyenne environ 64658 par jour)

*Nous verrons pourquoi le nombre de morts par MNT est plus important selon l’âge et surtout chez les personnes de plus de 70ans qui devraient mourir de « mort naturelle ».*

  L’OMS nous indique que parmi le nombre de décès, dans le monde, par an dus aux maladies non transmissibles, nous trouvons en premier :

- **Les maladies cardiovasculaires** y compris l**es accidents vasculaires cérébraux (AVC)** : 17,9 millions de morts par an, soit en moyenne plus de 49041 morts par jour. En France 140000morts par an (Ministère des solidarités et de la santé), soit près de 384 morts par jour.

- Puis viennent ensuite **les cancers** avec 9 millions de mort par an, soit en moyenne 24657 morts par jour. En France 150303 morts  par an (Santé Publique France, Inserm, Institut National du Cancer), soit 411 morts par jour. Pour la France les cancers prennent la première place.

- En troisième position, **les maladies respiratoires chroniques** : 3,9 millions de décès par an, soit en moyenne  10684 morts par jour. En France 34800morts par an (INSSE), soit en moyenne 95 morts par jour. 

- En dernier le **diabète** avec 1,6 millions de décès par an, soit en moyenne 4383 morts par jour. En France 34000 morts par an (Santé Publique France), soit en moyenne 93 morts par jour.

## II) Dogme allopathique.

Au regard de ces chiffres et suivant le dogme de la médecine allopathique, tous ces morts sont des victimes, de même pour les maladies infectieuses.

 **La maladie s’installe, la faute à qui ?**
 **A la maladie !** 

Le malade est devenu la victime :
- D’un organe défaillant (cœur, vaisseaux sanguins, foie, vésicule biliaire, reins, poumons, pancréas…….)
- Ou de méchants intrus que sont les microbes, selon leur classe (amibes, champignons, bactéries, bacilles, virus)
- Ou à cause de la nature : pollen, graminées
- Ou encore, d’un déséquilibre cellulaire : cellules cancéreuses…..
- Puis n’oublions pas aussi les mauvais gènes que l’on incrimine de déséquilibre organique. 

**Responsabilité**

De ce fait le malade est complètement déchargé de la survenue de la maladie et de son évolution. Il doit suivre scrupuleusement les instructions de la médecine allopathique, à travers le médecin, et se soumettre à la médication et opérations chirurgicales.

  Ainsi la médecine allopathique, à travers son corps médical, devient vitale pour la majorité des individus, dans un rôle qui serait obsolète (sauf exception, voir ci-dessous), si l’on enseignait dès l’école primaire jusqu’au lycée en terminale : à savoir gérer, en prenant en main par nous-mêmes, notre Santé Naturelle.    

  Cette politique dictatoriale sur la santé, par ce non enseignement, laisse la main mise aux trusts pharmaceutiques, par laquelle le médecin endosse un rôle de garant et parmi lesquels , nombreux sont ceux qui reçoivent de très bonnes rémunérations. 

## III) La médecine allopathique est-elle à sa place ?

### a) Où en sommes nous ?

- Le diagnostic et le choix du remède sont les clefs de cette pratique.

- La médecine allopathique ne s’attaque essentiellement qu’aux symptômes, négligeant la cause profonde, générale de la maladie, à travers des remèdes violents, qui ont été expérimentalement efficaces, mais toxiques pour l’organisme.

- Elle est une médecine symptomatologique, malgré sa théorie pasteurienne des agressions microbiennes.

- Par ses actions la médecine allopathique étouffe les forces de l’auto-défense organique, ainsi l’auto-guérison. Elle n’en tient absolument pas compte et de plus les ignore.

Le comble de la médecine allopathique, c’est qu’elle trompe le malade en lui faisant croire qu’il est guéri par l’étouffement des symptômes et en le laissant continuer ses erreurs de vie.

> C’est plutôt, en faisant comprendre aux malades qu’ils doivent changer une certaine manière de vivre, et, de leur enseigner le pourquoi la maladie c’est installée, par un mauvais environnement cellulaire (sérum extracellulaire), causant un déséquilibre cellulaire et engendrant le processus de la maladie. Pour aller véritablement dans une vraie guérison, par l’action de l’organisme dans sa démarche d’auto-guérison.

Mais là est tout le problème qui vient rejoindre l’enseignement de l’autogestion de Sa Santé Naturelle pour une véritable prévention de la Santé.

Ce problème sera résolu complètement, non pas par petit bout comme à l’heure actuelle, mais lorsqu’il y aura véritablement un changement de société, dans laquelle, l’individu sera respecté par la société et l’individu respectant la société. L’un ne va pas sans l’autre, pour un véritable équilibre sociétal et une harmonisation sociétale, dans laquelle aucune exploitation pour des bénéfices et des profits monétaires ne doit avoir lieu. Où chaque individu peut trouver sa place dans une Société à Echelle Humaine.  

## IV) La véritable place de la médecine allopathique.

Malgré ce que je viens d’écrire ci-dessus, la médecine allopathique a une utilité bien précise, dans les cas d’urgence qui sont :

- Les traumatismes dus aux accidents, qui ne sont pas des maladies. La médecine allopathique est très habille dans ce domaine.

- Les débordements microbiens, pour lesquels la cohorte d’antibiotiques va pouvoir stopper ces débordements.

- Les grandes douleurs (avec les antalgiques et les analgésiques)

- Les lésions importantes mettant la vie des personnes en danger (lésions anatomiques ou histologiques [tissus biologiques] d’un organe)

- Les maladies dégénératives (cancers, sclérose en plaque, maladie de Creutzfeld-Jacob, maladie d’Alzheimer, maladie de Parkinson………..)

> Comme le disait P.V Marchesseau (1911-1994, fondateur de la Naturopathie Contemporaine) :
>  
> **« Si la médecine allopathique n’existait pas, il faudrait l’inventer »**

Malgré tout, dans son véritable domaine, la médecine allopathique ne représente que 20% des cas.

Sur les autres 80% des cas, 30% peuvent-être alloué à la médecine homéopathique dans les quels la médecine allopathique n’a pas à intervenir et où l’intervention de la Naturopathie en tant que Conseil de Santé à travers l’Hygiène Vitale se trouve gênée pour le faire.

***Nota bene***
La médecine homéopathique reste sur le principe anti-symptomatique, sans aller atteindre la cause profonde du mal. Sa démarche est la réparation locale comme la médecine allopathique.
L’important, elle est pratiquement sans danger.

Ni elle, ni la médecine allopathique ne sont dans la démarche d’une véritable prévention des maladies par une hygiène de vie, et d’une régénération du potentiel vital humain pour les générations futures.

