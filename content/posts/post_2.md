---
weight: 1
title: "Base scientifique en naturopathie contemporaine - Partie 1/3"
date: 2020-05-26T21:40:32+08:00
lastmod: 2020-03-06T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["santé", "science", "naturopathie"]
categories: ["Base scientifique en naturopathie contemporaine"]
featuredImage: "/images/blog/rsz_pexels-photo-785695.jpg"

lightgallery: true

toc:
  auto: false
---


## L’humorisme théorie et pratique

### Avant Propos

 En plus de ce développement, si vous désirez approfondir (ce que je vous conseille) procurez vous un bon ouvrage de biologie (anatomie – physiologie), tel terminal de lycée ou à l’usage des candidats au concours des grandes écoles.
 
Ne vous étonnez pas de certaines conceptions émises par la Naturopathie Contemporaine et que vous lirez dans cet exposé sur la science naturopathique. Conceptions qui sont contraires à l’enseignement officiel donné dans ces livres. Ces divergences qui commencent par l’étude de la biologie cellulaire, continuent en physiologie, en hygiène, en pathologie et en thérapeutique.

Cette différence est dû, que la biologie naturopathique est vitaliste et humorale et non mécaniste et solidiste comme la biologie officielle.

### Connaitre Son Organisme 

Pour bien auto gérer sa Santé Naturelle, il est important de bien se connaître : physiologiquement, psychologiquement, et, sur l’état de son esprit.

Nous allons commencer par nous intéresser au fonctionnement de notre organisme. Tout en sachant qu’il est tributaire par son interdépendance avec notre mental (ensemble de nos pensées et de nos émotions) et de notre état d’esprit ; c’est-à-dire, si nous sommes, sous la dépendance de la colère, de la jalousie, de l’orgueil, de la haine, de l’attachement, de la cupidité, de la malveillance – c’est de constater sans se culpabiliser et agir de façon à diminuer les dépendances qui nuisent à notre santé naturelle - , ou, si nous développons la générosité, la patience, la persévérance, la vigilance, le moment présent et si nous évitons tout acte nuisible pour soi et les autres.

C’est par la biologie que nous allons commencer notre investigation de notre organisme.

##### **a) Origine et sens du mot biologie**  


Le terme biologie fut formé de manière indépendante courant fin XVIIIème siècle et début XIXème siècle, par Gottfried Reinhold Trevinarus (naturalisme allemand 1776-1837)
et Jean-Baptiste Lamarck (naturalisme français 1744-1829) : *«Tout ce qui est généralement commun aux végétaux et aux animaux comme toutes les facultés qui sont propres à chacun de ces êtres sans exception, doit constituer l'unique et vaste objet d'une science particulière qui n'est pas encore fondée, qui n'a même pas de nom, et à laquelle je donnerai le nom de biologie. » (Recherches sur l’organisation des corps vivants)*
Il est formé de la composition de deux mots grecs bios « vie » et logos « paroles discours, relation ». C’est la parole, le discours, la relation sur la Vie, que nous interprétons par **l’Etude de la Vie**.

Néanmoins la Vie nous ne pouvons pas la voir, la toucher. Nous ne pouvons voir et toucher que ses manifestations et les étudier. Sans oublier qu’une interdépendance les relie entre elles. Ce qui ne nous empêche pas d’en faire l’analyse tout en les replaçant dans leur globalité.

##### **b) Deux pratiques de la biologie**

  

- Biologie classique, mécaniste, s’appuyant sur les phénomènes physico-chimiques

  

- Biologie Naturopathique, tenant compte de la globalité de l’Etre Humain dans son milieu spécifique.

  

- **La biologie classique**
	La biologie classique ne tient pas compte des interdépendances qui relient ces différentes manifestations. Elle réduit son horizon d’étude aux seules recherches de laboratoire par spécialisations : examens des cellules, des organismes microbiens, des tissus, des analyses de sang, d’urines, recherche des principes actifs en chimie organique pour les reproduire en synthèse, expérimentations de médicaments en pharmacodynamiques sur les animaux, mutations génétiques (créations de chimères)

  

- **La biologie Naturopathique**
	Pour la Naturopathie Contemporaine, la biologie comprend certaines de ces recherches en dehors de la fabrication de médicaments de synthèse, de mutations génétiques. Elle s’appuie sur des notions beaucoup plus générales concernant les manifestations de la Vie et de leurs conditions. C’est plus par synthèse que par analyse qu’elle procède.

	Elle est avant tout la « ***Science de l’Etre Vivant et de ses conditions d’existence**, c’est-à-dire **des conditions du milieu spécifique** qui font naître les manifestations de l’Etre et assurent sa pérennité.*» ( P.V Marchesseau. Biologiste et fondateur de la Naturopathie Contemporaine)
	
Afin de maintenir ces manifestations en équilibre (Santé Naturelle), ou de les retrouver, la biologie naturopathique aboutit à proposer des règles de comportement correct, sans pour cela s’enfermer complètement dans un laboratoire pour étudier **les secrets des manifestations de la Vie**. Par contre en les observant dans la nature partout où elles se manifestent. Aussi bien que ces manifestations de la Vie soient végétales, animales ou humaines.

Les études biologiques en Naturopathie Contemporaine ont comme réalisation la **Santé Naturelle** et non pas la santé artificielle. Le but c’est de déterminer la nature de cette « santé ». Suivant le résultat, comment la conserver, la développer au maximum par rapport à notre hérédité et la restaurer lorsqu’une défaillance organique commence à s’installer.

Tandis que le biologiste classique-mécaniste qui, penché sur ses paillasses de laboratoires, base ses recherches sur un principe mécaniste de l’organisme, jusque dans la cellule, en réduisant le vivant aux lois physico-chimiques, espère protéger la santé humaine ou de la restaurer en créant des procédés, également contre les agresseurs jugés responsables de certaines maladies, ou en créant des molécules de synthèse pour maintenir une santé artificielle. Il est au service des multinationales pharmacologiques et de la médecine allopathique.

A **l’inverse** la Biologie Nathuropathique, afin de mieux comprendre un « Etre » l’examine dans sa totalité que dans ses parties qui ne sont jamais révélatrices des **ressources vitales disponibles**. Suivant le principe que la globalité l’emporte sur le détail, c’est par l’étude de la **sagesse du corps**, en observant les lois de la manifestation de la Vie que la Naturopathie Contemporaine propose, sans avoir recours à la magie des principes chimiques actifs, de conserver, d’améliorer au maximum et de retrouver (en dehors des maladies extrêmes et des accidents graves) une **Santé Naturelle**, et, non une santé artificielle. En étant le plus possible **dans un accord de l'Être Humain avec son milieu spécifique**.

Au sujet de ce milieu spécifique, quatre questions viennent en tête.
- Quel est ce milieu spécifique à l’Etre Humain ?
- Quels sont les erreurs de vie nous éloignant de ce milieu spécifique ?
- Comment retrouver ce milieu spécifique ?
- Si retour impossible, comment appliquer **des mesures compensatrices naturelles ?**

C’est ce que nous verrons au cours de notre étude.

**A suivre , Partie 2:  Qu'est-ce que notre organisme ?**

***Bonne lecture.
J.L***