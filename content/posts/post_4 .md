---
weight: 1
title: "Base scientifique en naturopathie contemporaine - Partie 3/3"
date: 2020-08-02T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["santé", "science", "naturopathie"]
categories: ["Base scientifique en naturopathie contemporaine"]
featuredImage: "/images/blog/rsz_pexels-photo-1029604.jpg"

lightgallery: true

toc:
  auto: false
---


## Pourquoi en Naturopathie Contemporaine, il est dit que la cellule est intelligente ?
Faisons un rapprochement avec notre cerveau. Notre cerveau nous permet d’avoir la capacité de provoquer des comportements dans l’interaction avec notre environnement. Par ce fait nous parlerons d’intelligence.
Nous avons vu plus haut que nos cellules, par l’intermédiaire de leur membrane, réagissaient aux interactions avec l’environnement intra et extracellulaire. Comme nous le faisons avec notre cerveau par rapport à notre environnement.
Par ce fait nous pouvons dire que nos cellules ont un cerveau et s’il y a cerveau, il y a **intelligence**. Que leur cerveau, **est leur membrane** et non pas le noyau comme le dit la biologie classique.

## Pour quelle raison c’est la membrane le cerveau et pas le noyau ?

Par expérience si on retire le noyau de la cellule, celle-ci continue à vivre pendant un certain temps. Ce qui permet de faire des transferts de noyaux (clonage).
Dans le cas de la membrane cellulaire, si l’on détruit celle-ci, la cellule meurt, tout comme nous mourrons si l’on détruit notre cerveau. De plus, si on laisse la membrane intacte, mais que l’on détruise les PMI effectrices, en laissant les PMI réceptrices, la cellule entre dans un état coma profond. Il en est de même si l’on fait l’expérience sur les PMI réceptrices en conservant les PMI effectrices.
Pour vivre et avoir un comportement « intelligent», les cellules ont besoin d’une membrane complète avec toutes PMI.
Pour qu’il puisse y avoir intelligence, il faut une conscience, puis agir en rapport à cette conscience.

Les PMI réceptrices percevant les informations venant des milieux extra et intracellulaire, peuvent être assimilées à la **« conscience »** des cellules.
Les PMI effectrices, par leur **«action»** agissent à la suite de cette «conscience» cellulaire : perceptions des PMI réceptrices.
En accord avec le fonctionnement de l’intelligence, Pour la Naturopathie Contemporaine, ce binôme constitue **l’intelligence cellulaire**.

## Le milieu humoral de notre organisme

Notre  organisme est une véritable baignoire. Il est constitué de 60 à 70% de son poids en eau. De ce pourcentage environ 80 à 85 % se situent dans nos cellules. Ils représentent en biologie naturopathique le **sérum intracellulaire.** Le reste du pourcentage en eau est nommé le **sérum extracellulaire**.

> **Les sérums extra et intracellulaire avec la lymphe, le sang et son plasma constituent le milieu humoral de notre organisme, représentant la base scientifique de la Naturopathie Contemporaine : l’Humorisme.**
> 
## L’importance de ce milieu humoral
Les cellules absorbent les éléments dont elles ont besoin pour vivre, se trouvant dans leur environnement, c’est-à-dire dans le **milieu extracellulaire.**
> **C’est dans ce milieu extracellulaire que se déroulent les échanges entre la cellule et le milieu extérieur et entre elles.**

L’existence de nos cellules et leur activité **dépendent de leur interaction avec leur environnement** et non de leur code génétique comme le veut la biologie officielle.
La raison en est, pour se maintenir en vie les cellules (aussi bien que nous) dépendent de la capacité de s’adapter aux changements constants de l’environnement. Par ce fait, les gènes ne peuvent programmer à l’avance la vie des cellules, donc de notre organisme.
Suite à la transformation de nos aliments par notre système digestif, les nutriments qui en ressortent, ainsi que l’oxygène absorbé par notre système respiratoire, vont se retrouver dans ce milieu humoral.
Tous les apports nutritifs et l’oxygène dont les cellules ont besoin, se retrouvent dans le sérum extra cellulaire ; ils sont détectés et reconnus par les Protéines Membranaires Intrinsèques réceptrices, puis dirigés par les Protéines Membranaires effectrices dans le liquide intracellulaire (sérum intracellulaire). Ces deux PMI étant insérées dans la membrane cellulaire.
Nous y trouvons également dans le sérum extracellulaire, suite au métabolisme cellulaire, les déchets évacués par les cellules, toujours par ces mêmes protéines.
C’est également le lieu de transmission de vibrations dues à nos pensées, de messages entre cellules qui établissent leurs actions coordonnées.

## Composition du milieu extracellulaire : sérum extracellulaire

> **C’est une composition précise qui permet la bonne existence de nos cellules.**

Qui est constituée :  
- De **liquide interstitiel** ou **lymphe interstitielle**. Il est directement au contact des cellules.
- De **plasma sanguin**, c’est la partie liquide du sang à l’intérieur des vaisseaux sanguins.
- De **liquide transcellulaire** formé à partir du plasma (liquide céphalo-rachidien, liquide articulaire, liquide séreux……….)

Dans ce milieu qui baigne nos cellules se trouvent également les nutriments (glucose (sucre), sels minéraux, acides gras, acides aminés), des hormones, des coenzymes, des neurotransmetteurs et les déchets cellulaires dus à leur métabolisme.


![alt text](/images/blog/shema2.png "Schema")