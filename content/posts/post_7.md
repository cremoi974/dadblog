---
weight: 1
title: "La santé naturelle et la maladie en naturopathie contemporaine: Troisième partie"
date: 2021-01-14T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["maladie", "étude", "naturopathie"]
categories: ["La sante naturelle et la maladie en naturopathie contemporaine"]
featuredImage: "/images/blog/post_7_banner.jpg"

lightgallery: true

toc:
  auto: false
---

## I) INTRODUCTION

La bonne santé, la bonne vitalité de nos cellules et par corrélation de notre organisme, dépendent de deux principes fondamentaux:

* Assimilation
* Elimination

## 1) L'assimilation

a) Une bonne asimilation des nutriments spécifiques à nos cellules, ainsi qu'une bonne assimilation énergétique, leur donnent, de même qu'à nos tissus , à nos organes et par conséquence à notre organisme, **une vitalité quantitative importante.**

b) Une assimilation défectueuse en nutriments (glucides, protéines, minéraux, vitamine, oxygène, hormones et en énergie), dû à un mauvais apport en aliments spécifiques à l'être humain, ou par un mental perturbé, ou un état d'esprit dominé par un égo ou bien les 3 en même temps, **provoque une diminution de la vitalité.**

##  2) L'élimination

Un ralentissement de l'élimination par la rétention de l'eau, de l'urée, de la bile, de l'énergie...., dû à un mauvais fonctionnement des émonctoires (reins, poumons, intestins+foie et vésicule biliaire, peau) par intoxiation des cellules ou par blocage du diencéphale (provoqué par les mêmes phénomènes que lors d'une assimilation défectueuse) **entraîne un empoisonnement de l'organisme et amène des altérations qualitatives.**

>>*La mort partielle est le fidèle gardien de la vie intégrale. Seule la destruction perpétuelle du contenu des cellules, des tissus, des organes et de l'organisme entier garantit **la reconstruction** permanents des cellules, des tissus, des organes entiers.* 
>>(Docteur Alexandre Salamanoff: Secrets et sagesse du corps)

Pour bien comprendre le bon maintient d'une santé naturelle, nous allons partir des cellules qui nous constituent.
Nos cellules, bien qu'elles puissent choisir, accepter les substances nutrutives nécessaires et rejeter les déchets dû à leur métabolisme, pour leur entretien et leur prolifération, **doivent tenir compte du milieu environnemental dans lequel elles vivent**

Dans ce milieu environnemental, que nous nommons en Naturopathie **le sérum extracellulaire** (voir schéma précédent), les cellules doivent faire face aux déchets produits au cours de leur métabolisme (dioxyde de carbone, eau et molllécules organiques),aux corps étrangers qui ont réussi à atteindre le milieu extracellulaire, ainsi qu'aux millions de micro-cadavres cellulaires qui s'y forment à chaque instant.

## II) REVENONS UN PEU SUR LA BIOLOGIE CELLULAIRE

## 1) La notion de vie

Notre constitution est le résultat de processus évolutifs engendrés depuis plusieurs milliards d'années.
Nous sommes le résultat d'un assemblage commençant par les atomes:

**Atomes -> Molécules -> Organites -> Cellules -> Tissus -> Organes -> Systèmes -> Organisme**

La notion de vivant se caractérise par:
* La capacité de se reproduire;
* La croissance, le développement;
* La capacité de produire et d'utiliser l'énergie indispensable à l'intégrité des fonctions cellulaire et organique;
* La capacité d'adaptation;
* La faculté de se maintenir en vie dans les limites du raisonnable (homéostasie)

Tous ces critères se retrouvent à travers l'existence de nos cellules.

## 2) Physiologie de la cellule

Nous avons vu précédemment la constitution de la cellule avec sa membrane plasmatique qui est également **son cerveau, déterminant les substances qui peuvent pénétrer en elle ou en sortir et aussi échanger de l'information avec l'extérieur**.Et son cytoplasme dans lequel est contenu le noyau où se trouve l'essentiel du matériel génétique de la cellule (ADN), ainsi que les différents types d'organites qui accomplissent chacun une fonction essentielle au maintient de la vie des cellules et de leur production.

## 3) Types de cellules

Nous sommes constitués de deux types de cellules.

**a) Les cellules inanimées ou fixes** 
Elles sont les éléments constitutifs des différents tissus. 
*Quoi qu'elles portent ce nom. Il y a mouvement mais lents. Elles glissent dans leur milieu comme l'huile s'étendant sur l'eau.*

**b) Les cellules animées**
Vivant dans toutes les parties corps, **elles remplissent le double rôle d'y entretenir la vie en même temps qu'elles la défendent.**
Douées d'une activité prodigieuse, **elles entrent en lutte avec tous les éléments pouvant être nuisibles à la bonne santé de l'organisme.**
Ce travail de défense et de nettoyage s'appelle **phagocytose**. Par cette manifestation **elles ingèrent et digèrent les éléments nocifs à notre organisme.**
Ces cellules animées sont **les globules blancs** ou **leucocytes**; subdivisés en: *basophiles, neutrophiles, éosinophiles, lymphocytes et monocytes.** (Ils seront étudier avec le système circulatoire).

> > **Quiconque jouit d'une bonne phagocytose, n'a pas à craindre les maladies contagieuses, ni les infections quelconques.
> > D'où l'importance que nos cellules se trouvent dans un milieu et un environnement convenable pour leur existence.**

Si elles ne trouvent pas les éléments de régénération qui leur sont indispensables, les cellules animées finissent par disparaître. Ces éléments régénérateurs ne se trouvent **que dans les aliments frais, principalement ceux spécifiques à l'être humain.** 
Les aliments altérés par des actes destructeurs ne peuvent plus accomplir complètement leur rôle de régénération (Nous verrons cela dans l'étude sur l'alimentation: la Bromatologie.)

## 4) Division cellulaire ou mitose

Toute cellule provienne d'une cellule préexistante. Dans chaque nouvelle cellule le contenu génétique se réparti de manière équivalente à partir de chaque cellule mère.

>> **Important - Une cellule pourra se diviser si**:
>>- ***elle n'a pas manqué de nutriment***
>>- ***elle n'est pas en contact avec certains poisons qui bloquent la division***
>>- ***sa grandeur devient critique***
>>- ***elle reçoit un message venant d'autres cellules***
>>- ***les cellules environnantes meurent (brûlure, coupure entraînant la mort de très nombreuses cellules)*** 
>>- ***elle reçoit l'ordre de se diviser***

![Schéma division cellulaire ou mitose](/images/blog/post_7_schema_1.png "Schéma division cellulaire ou mitose")

Il y a deux sortes de mitose

**a) Mitose réductionnelle**
Qui n'a lieu qu'au moment de la fécondation; indispensable pour retrouver le nombre des 46 chromosomes caractéristiques.

**b) Mitose équationnelle**
Qui a lieu tout le long de notre existence. Elle est caractérisé par la duplication de tous les éléments constitutifs de la cellule (*cytoplasme, organites, noyau et chromosomes.)* et par la répartition de ceux-ci dans les nouvelles cellules filles, absolument identiques à la cellule mère.
Lors de la reconstruction, les deux cellules filles retrouvent un noyau avec le même nombre de chromosomes (46) et d'organites.
Parmi ces deux cellules fille, une reste cellule fille et une autre devient une nouvelle cellule mère et reproduira à son tour deux cellules filles. Tandisque l'autre cellule devient stérile.

La mitose équationnelle dure généralement de 30mn à 3 heures.

>>**Important**
>>*Sans dérèglement ni perturbation dans l'organisme, dépendant de nombreux facteurs*, ***les cellules se divisent et se reproduisent à un rythme harmonieux.***
>>*En cas de perturbations de la santé naturelle par différents facteurs extérieurs - physiques et psychiques -*
>>
>>***a) Les cellules se fatiguent et meurent prématurément.*** *Nous avons à faire à un vieillissement cellulaire avant terme.*
>>*La lenteur de cicatrisation est un signe de faiblesse dans la multiplication cellulaire dû à cette fatigue des cellules correspondantes.*
>>
>> ***b) Devant les agressions répétées que subissent nos cellules, elles se réorganisent*** *pour se reproduire à un rythme accéléré en se mutant **pour donner que des cellules mères au lieu d'avoir une cellule fille stérile et une cellule mère reproductrice**, dans une progression normale et arithmétique:*
>> *Une cellule -> 2 cellules -> 5 cellules -> 7 cellules -> 9 cellules...Avec toujours la même constante.*
>>         
![Schéma multiplication cellulaire normale](/images/blog/post_7_schema_3.png "Schéma multiplication cellulaire normale")
>>
>> 
>>***Elles se multiplient au rythme d'une progression géométrique:***
>> *1 cellule -> 2 cellules -> 4 cellules -> 8 cellules -> 16 cellules -> 32 -> 64 -> 128 ->256 -> 512...* ***Ce qui provoque les tumeurs cancéreuses***
>>
>>![Schéma mutation cellulaire après agression répétées](/images/blog/post_7_schema_2.png "Schéma mutation cellulaire après agression répétées")


## 5) Etude bio-physico-chimique de la cellule

La cellule est le siège de phénomènes bio-physico-chimiques importants.

### A) Composition chimique de la cellule
* 4/5 de son poids en eau
* De sels minéraux, en faible proportion (Chlorures, phosphates, sulfates de sodium, de potassium, de calcium, de magnésium...)
* De l'oxygène
* Du dioxyde de carbone
* De substances organiques: protides, lipides, glucides

**LES SUBSTANCES ORGANIQUES**
* **1) Les protides**
Composés de 4 éléments principaux: *carbone, hydrogène, oxygène, azote.* Ils constituent 96% des molécules des cellules.
Les protides sont appelés encore **albuminoïdes**. Ils sont constitués de grosses molécules qui se scindent en molécules plus petites:
**En acides aminés** le plus grand nombre
Et peptides

Les acides aminés sont **solubles dans l'eau**. Ils possèdent **une fonction acide** et **une fonction basique.**
On distingue 2 groupes de protides.

* ***Les holoprotéines*** constitués uniquement d'acides animés.
* ***Les hétéroprotéines*** constituées d'acides animés et d'une autre substance, molécule non protéique renfermant du phosphore, ou un métal (*fer dans l'hémoglobine du sang*).
* **2) Les lipides ou corps gras** (du grec lipos, graisse)
Les lipides sont constitués de *carbone, d'hydrogène, d'oxygène.* Elles sont insolubles dans l'eau. Elles sont solubles dans l'alcool, éther, le sulfure de carbone, l'acétone, le chloroforme.
Elles possèdent l'une ou l'autre des caractéristiques suivantes:

>>* **Source d'énergie et de vitamines**
>>* **Constituants majeurs des membranes cellulaires**
>>* **Vecteur d'information comme hormones ou messagers intracellulaire**

Leurs molécules sont beaucoup plus petites que celles des protides.
Elles renferment toujours un ou plusieurs acides gras (*Oléique, stéarique..)*
On distingue deux groupes de lipides
* ***Les lipides simples***, qui renferment uniquement des acides gras avec un ou plusieurs alcools (*huile d'olive, beurre*)
*  ***Les lipides complexes***, qui renferment un ou plusieurs acides gras, un corps à fonction alcool multiples, une ou plusieurs bases azotées et quelquefois de l'acide phosphorique ou de l'acide sulfurique.
On distingue:
1. *Les phospholipides*, constituant des membranes cellulaires
2. *Les lipoprotéiques*, constitués de protides et de lipides; sont présentes dans le plasma sanguin, les tissus nerveux et le cerveau.

* **3) Les glucides ou hydrates de carbone**
Ce sont les molécules les plus abondantes à la surface du globe.
Des dérivés de glucides se retrouvent dans un grand nombre de molécules biologiques, comme les acides nucléiques ADN et ARN.
Les glucides ont pour type les **sucres**, en particulier le **glucose**. Ils sont constitués de ***carbone, d'hydrogène, d'oxygène***.
Les principaux glucides sont:

    - ***Monosaccarides***, glucose, lévulose, lactose.....
    - ***Disaccharides***, saccharose, maltose...
    - ***Polysaccharides***, glucides à grosses molécules, glycogène, l'amidon des cellules végétales.

>> ***Monosaccharides et disaccharides sont solubles dans l'eau, les polysaccharides sont insolubles***

### B) Polarisation cellulaire
La polarisation est la présence de charges électriques opposées de chaque côté de la membrane cellulaire. 
Elles sont positives à l'extérieur de la cellule et négative à l'intérieur de celle-ci.
On dit que la cellule est **polarisée électriquement**.
**Ces charges disparaissent après la mort du cytoplasme**
* Le milieu intracellulaire, *le cytoplasme*, **est chargé négativement** avec une présence en majorité ***d'ions K+(potassium)***
* Le milieu extracellulaire, *le secteur interstitiel (liquide composé de 90% d'eau)*, qui se trouve entre les cellules, permettant les échanges de nutriments vers les cellules et les déchets de celles-ci avec les capillaires sanguins, **est chargé positivement** avec présence en majorité ***d'ions Cl-(chlore) et d'ions Na+(sodium)***

![Schéma de la polarisation cellulaire](/images/blog/post_7_schema_5.jpg "Schéma de la polarisation cellulaire")

>> ***La fonction de chaque cellule est déclenchée par sa polarisation.***

Le phénomène de la polarisation passe par deux phases.
1. ***La dépolarisation et potentiel d'action***
Par l'intermédiaire **des canaux "Na+"** dans sa membrane, la cellule ouvrant ceux-ci, permet aux ions sodium Na+ du milieu extracellulaire **de pénétrer dans son milieu intracellulaire.**
Par ce fait les ions sodium Na+ ***se trouvent en surnombre dans le milieu intracellulaire*** par rapport aux ions potassium K+ et ***la cellule devient positive***. Sa charge électrique étant modifiée, elle est donc **dépolarisée**. ***Ce qui rend aussi le milieu extracellulaire négatif***.
1. ***La repolarisation***
La cellule ne peut avoir un nouveau potentiel d'action que lorsque l'équilibre de polarisation de celle-ci est retrouvé. C'est-à-dire ***que l'intérieur de la cellule doit redevenir négatif et l'extérieur positif***.
Pour cela la membrane cellulaire ***ouvre de nouveau les canaux à sodium Na+ mais également les canaux à potassium K+.*** Ceux-ci étant appelés **pompe à sodium-potassium** qui permettent aux ions sodium Na+ en surplus **d'être évacués vers l'extérieur de la cellule** et **aux ions potassium K+ de pénétrer à l'intérieur de celle-ci**.

>> ***L'action des pompes à sodium-potassium se fait jusqu'à ce que la charge appropriée soit atteinte à l'intérieur de la cellule.*** C'est-dire ***que la concentration en ions potassium K+ soit environ 10 à 20 fois supérieur à celle de l'extérieur de la cellule. Tandis que la concentration d'ions sodium Na+ redevient beaucoup plus élevé en dehors de la cellule***

![Schéma de la polarisation cellulaire](/images/blog/post_7_schema_4.png "Schéma de la polarisation cellulaire")

>>*Bien que cette fonction soit présente dans toutes les membranes des cellules*. ***Elle joue un rôle dans le maintient du potentiel de repos des cellules nerveuses, musculaires et cardiaques***

### C) Perméabilité sélective de la cellule

Toute cellule absorbe des aliments (métabolisme digestif), certaines absorbent des corps étrangers à détruire (phagocytose).

Le cytoplasme des cellules est **le siège de réactions biochimiques forts complexes**.
Certains produits qui en résultent **sont inutiles et même nuisibles; elle les rejette**.
Ces excrétions traversent la membrane cellulaire et se retrouvent dans le sérum extracellulaire.

>> *La cellule fait un choix (***intelligence cellulaire***) dans les éléments qu'elle absorbe, comme ceux qu'elle retient et rejette*. ***Sa perméabilité est sélective***.
>> *Les propriétés de la membrane cellulaire sont intimement liées à l'activité, à la vie de la cellule tout entière*, ***dans l'environnement dans le quel elle se trouve***.

### D) Les Hormones
Issu du mot grec "*ormon*" évoquant: mettre en mouvement.
Les hormones sont des substances chimiques **synthétisées par certaines cellules**, structurant des glandes spécialisées qui forment le système endocrinien.
Les hormones sont secrétées dans le sang ou la lymphe et transportées dans tout le corps suivant la demande cellulaire.
Elles vont se fixer soit sur les membranes à l'extérieur des cellules, soit à l'intérieur des cellules en traversant les membranes.

>>**Les hormones sont essentielles pour que le système cellulaire soit toujours en harmonie, de manière à garantir l'équilibre fonctionnel de notre organisme.**
>>***Cet équilibre, cette harmonie correspond à notre force vitale.***

>>***Lorsque les cellules hormonales secrètent trop peu ou trop d'hormones, il y a déséquilibres hormonaux. Et si ces sécrétions viennent à manquer, la situation est grave.***

## III) CONCLUSION
La compréhension de l'approche à la biologie cellulaire, nous aide à comprendre le phénomène de **la Maladie sous ses différents aspects**.

Mais également l'aide que nous pouvons apporter à notre organisme par **différentes pratiques naturelles dans notre quotidien pour qu'il puisse s'autoguérir** et retrouver notre Santé Naturelle.
