---
weight: 1
title: "La santé naturelle et la maladie en naturopathie contemporaine: Quatrième partie"
date: 2021-04-14T21:40:32+08:00
draft: false
author: "Jacques Lefort"

tags: ["maladie", "étude", "naturopathie"]
categories: ["La sante naturelle et la maladie en naturopathie contemporaine"]
featuredImage: "/images/blog/post_8.jpg"

lightgallery: true

toc:
  auto: false
---

Afin de pouvoir bien comprendre le processus de la maladie en Naturopathie Contemporaine, il faut revenir sur le monde cellulaire qui constitue notre organisme.

Nous avons, grâce aux microscopes électroniques, en plus des différentes cellules qui composent nos tissus organiques, tout un monde microbien cohabitant avec celles-ci.
Parmi cette communauté de micro-organismes, nous trouvons des bactéries, des levures, des champignons, des archées, des virus. Cet ensemble de micro-organisme, appelé avant flore microbienne a pris le nom de microbiote, **intéragit en permanence avec notre milieu cellulaire, en passant par l'intermédiaire des systèmes: sanguin, immunitaire et nerveux.**
  
Notre organisme héberge plusieurs microbiotes dans différents endroits:
  
  - La sphère ORL (bouche,nez,oreilles)
  - La peau
  - Les poumons
  - Les intestins (c'est le microbiote le plus développé de l'organisme, environ 100 000 milliards de micro-organismes)
  - Les voies urinaires
  - Et chez la femme, le vagin (les micro-organismes qui peuplent le vgin sont appelés: flore vginale)
  
Notre organisme contient une moyenne de 30 000 milliards de cellules; mais **10 fois plus de cellules microbiotiques cohabitent avec celles-ci** à travers des différents organes structurant notre organisme.
  
*Nous verrons plus-tard ces micro-organismes constituant nos différents microbiotes* ***qui ont une fonction importante dans le maintient de notre Santé Naturelle.***
  
Nous pouvons dire,par cette mixité de nos cellules avec le monde microbien constituant nos microbiotes, **que notre organisme est un véritable écosystème qui doit être tenu en équilibre.**


##     I) QU'EST-CE UN ETRE VIVANT?

Il est dit, pour qu'un Etre soit vivant, et, qu'il ait une forme de vie individuelle, il lui faut des organes qui accomplissent des fonctions pour maintenir celui-ci en vie, de la naissance à la mort. Ces organes lui permettent à travers son cycle de vie complet, de **réagir à son environnement et de se reproduire**.

Si nous réfléchissons sur le monde cellulaire qui constitue notre organisme, nous pouvons avancer en tenant compte de ce qui est dit, sur ce qu'est un "Etre vivant", **que chaque cellule est un Etre Vivant,** sachant que celle-ci comporte des organes appelés "**organites"**. Donc comme chaque "Etre vivant", ***elles sont tributaires de leur environnement tout en ayant également un rôle sur celui-ci.***

A la suite de cette remarque, nous pouvons comparer comment la médecine et la naturopathie conçoivent la Maladie et la Santé.


## II) EXAMINONS QUELQUES DEFINITIONS

## 1) Définition de la Maladie
a) Suivant le dictionnaire Larousse: *Altération de la santé, des fonctions des êtres vivants.*
b) Suivant le Petit Robert: *Altération organique ou fonctionnelle considérée dans son évolution ou comme une entité définissable.*
c) Suivant l'OMS: *Dysfonctionnement d'origine physique, psychologique ou/et sociale, qui se manifeste sous différentes formes.*

Arrêtons-nous sur les deux termes, ***altération*** dans la définition du Larousse, et, ***dysfonctionnement*** dans la définition de l'OMS.

- ***Altération***: Une altération c'est la modification, le changement qui dénature l'état normal.
- ***Dysfonctionnement***:Un dysfonctionnement c'est une annomalie de fonctionnement.

Ainsi par rapport à ces deux définitions, la médecine et dansle langage courant , il est dit qu'une personne ***a une mauvaise santé, n'a pas une bonne santé***. **Mais très rarement qu'elle a perdu la santé**.

Tandis qu'en Naturopathie nous parlerons **de perte de santé,** dans le sens de **Santé Naturelle, qu'il faut retrouver par des moyens naturels propres à l'Etre Humain.** Et non pas de santé artificielle soutenue par de multiples médicaments ou différents actes médicaux.

## 2) Définition de la santé
a) Suivant le dictionnaire Larousse: *Etat de bon fonctionnement de l'organisme.*
b) Suivant l'OMS: *La santé est un état de complet bien-être physique, psychique et social et ne consiste pas seulement en une absence de maladie ou d'infirmité.*

La définition du Larousse s'arrête qu'au fonctionnement de l'organisme, comme si celui-ci était une machine.

L'OMS par sa définition se rapproche d'une vision globale de la santé, en insérant dans sa définition, ***physique, psychique et social.*** Mais deux éléments importants sont négligés: "***l'Etat d'Esprit"*** qui intervient dans la constitution de chaque personne, et, "***le Milieu Environnemental***", dans lequel chaque individu beigne depuis sa naissance à sa mort.

C'est en tenant compte de ces cinq éléménts (*physique, psychique, ***état d'esprit***, social et ***environnemental***) que nous pouvons parler d'une **vision globale de la santé.**

c) Pour la Naturopathie, la Santé Naturelle se situe dans une vue globale de l'Etre Humain. **C'est le résultat de l'interdépendance en équilibre**: de la ***puisssance génétique,*** de la ***puissance biologique-physiologique, de l'état psycho-affctif, de l'état d'esprit, du milieu sociétal,*** et, ***environnemental.***

>>***Nota:*** *Ce qu'il faut prendre consciene, c'est que l'Etre Humain* ***ne vit pas dans son milieu naturel, il est coupé de ses racines.***
>>- *Exploitation de celui-ci par le système travail*
>>- *Prise du pouvoir sur la vie par "l'empereur" argent*
>>- *Destruction de la nature* 
>>- *Déversement de produits chimiques, radioactifs, nanoparticules....dans l'air, l'eau, la terre, la nourriture, notre oganisme...*

>>*Pour la Naturopathie,* ***la Santé est maintenue ou défaite en fonction des éléments normaux ou anormaux** *qui se trouvent dans ce milieu.*
>>*Les éléments* ***anormaux*** *sont ceux qui ne* ***sont pas spécifiques, ni prévus***, *pour les êtres humains.*


## III) LA MALADIE ET LA SANTE VUES PAR LA MEDECINE

## 1) La médecine allopathique
C'est la forme officielle dans le milieu médicale. Elle se trouve sous la tutelles des laboratoires pharmaceutiques.

Elle est basée **sur l'étude de la maladie locale qu'elle considère comme le mal en soi.** Afin de retrouver la "*dite santé*", à la suite du diagnostic, elle va s'appuyer sur **la répression des symptômes** par l'action de remèdes, souvent très violents, dangereux par leur toxicité.

>> Les piliers de cette médecine allopathique sont: **diagnostic et le choix du remède médicamenteux.**

**a) Critique**
La médecine allopathique ne traite pas **la cause profonde, générale du mal qui s'installe,** mais seulement des **symtômes** qui pour elle indique la maladie.
***La médecine allopathique a une conception symptômatique de la maladie.***

Par ce fait de la répression des symptômes, elle **étouffe les forces de l'autodéfense organique et ainsi de l'autoguérison,** tout en maintenant dans l'organisme ***les toxines, en y ajoutant d'autres*** par l'intermédiaire de médicaments agressifs.
De plus par cette répression des symptômes, le malade est trompé croyant être guéri (*santé artificielle*) tout en pouvant **continuer ses erreurs de vie.**

**b) Place de la médecine allopathique**
Malgré les critiques ci-dessus, elle a une place **indispensable.** Cette place est celle de ***l'urgence est des cas exceptionnels:*** les accidents, les grandes douleurs, les lésions importantes (mettant la vie de la personne en danger), les débordements microbiens, les maladies "dites" irréversibles,....
Hors de ces cas d'urgences, l'allopathie n'a pas lieu d'être.

## 2) La médecine homéopathique
Suite aux abus de la médecine allopathique, Samuel Hahnemann (1755-1843), médecin allemand (chimiste, pharmacien et professeur d'université) chercha un moyen thérapeutique moins agressif et par la suite **fonda l'homéopathie.**

Technique médicale qu'il mit sur pieds, à la suite d'essais qu'il fit sur lui en prenant du quinqina, qu'à l'époque un médecin écossais (William Cullen) recommandait comme remède. 
De façon à évaluer ce médicament qui donne à une personne en bonne santé des symptômes équivalents à ceux de la fièvre intermittente. Il l'expérimente sur lui. Après plusieurs expériences sur lui, il constate qu'en prenant du quinqina la fièvre apparait et dès qu'il cesse d'en prendre, celle-ci disparaît.

S'appuyant sur la loi de similitude énoncée par Hippocrate: "*La maladie est produite par les emblables et par les semblables que l'on fait prendre revient de la maladie à la santé. Ainsi ce qui produit la Strangurie qui n'est pas, enlève la Strangurie qui est; la toux, comme la Strangurie est causée et enlevée par les mêmes choses."*, et, reprise par Paracelse au XVIème siècle: "*Les noms des maladies ne servent pas pour l'indication des remèdes; c'est le semblable qui doit être comparé avec le semblable et cette comparaison sert à découvrir les arcanes pour guérir*", Hahnemann établit la base fobamentale de l'homéopathie:"***Pour guérir une maladie, il faut administrer un remède qui donnerait au malade, s'il était bien portant, la maladie dont il souffre***". Enonçant par là, que les maladies sont guéries par les remèdes qui provoquent les mêmes symptômes que celles-ci.

Par ce principe de similitude, et ***en diluant presque à l'infini des drogues qui provoquent le mal que l'on veut enrayer***, Hahnemann développa ***un moyen thérapeutique pratiquement sans danger.***

A la différence de l'allopathie, les remèdes sont prescrits après une étude minutieuse de la maladie vue à travers une investigation approfondie du malade, de sa constitution, de son tempérament...

*Références: 
- medarus.org
- homeoint.org
- homéopathiepratiqu.fre.fr

**a) Critique**
Malgré sa non violence thérapeutique, l'homéopathie reste une médecine symptomatique, n'atteignant pas le mal profond.
Plus soucieuse dans son approche du malade que la médecine allopathique, le malade une fois guéri (*santé artificielle*) peut également continuer ses erreurs de vie.

**b) Place de la médecine homéopathique**
Lorsque l'allopathie ne devrait pas intervenir et que la naturopathie est gênée pour le faire,la médecine homéopathique a lieu d'être. Dans le cas ***de voyages, de maladies chez les très jeunes enfants ou les personnes âgées...

>> ***c) Nota*** : Nous pouvons constater que ces deux formes de médecines (allopahie et homéopathie) n'ont pas de méthode ***pour conserver le plus possible la Santé Naturelle,et, l'améliorer pour les générations futures.***
>> L'allopathie par sa **théorie microbienne des agressions** pense que par des techniques de **stérilisation et de vaccination**, elle offre une protection.
>> Pour l'homéopathie c'est par l'intermédiaire **de dilutions** (médicaments homéopathiques) que l'on parle de prévention.
>> La "prévention" allopathique et homéopathique restent toujours sur le principe symptomatologique. **Elle ne prend absolument pas l'Etre Humain dans sa globalité.** Il n'y a aucune action pour nous remettre le plus près et le mieux possible **dans notre contexte naturel.**
>> Par leurs actions, elles peuvent "***bouster***" temporairement l'immunité, mais elles **n'accroissent pas notre immunité naturelle**, permettant à notre organisme de faire face et de réagir à toutes pénétrations inadéquates à l'équilibre de celui-ci. 

>>***Constatation***: En allopathie comme en homéopathie rien n'est édifié véritablement pour une **véritable prévention naturelle** et encore moins pour **revaloriser et augmenter la puissance vitale** chez les individus pour en **faire bénéficier les générations futures.**


## IV) LA MALADIE ET LA SANTE VUE PAR LA NATUROPATHIE

Lorsque nous prononçons le mot maladie, ***nous entendons en premier "mal".*** Lorsque nous le lisons, ***nous prononçons en premier "mal".*** Ainsi en le décomposant nous pouvons remarquer l'expression ***le "mal a dit".*** Par ce fait, nous pourrions dire que notre organisme s'exprime sur l'agression qu'il subit.

Nous avons vu que la conception de la maladie par l'allopathie et l'homéopathie est **symptomatique.** Si ***l'on supprime le symptôme***, pour ces deux médecines, ***la maladie est supprimée*** et il y aguérison et retour à la santé.

>Pour la naturopathie, ce n'est qu'une **santé artificielle,** pour la raison que le mal profond **n'est pas stoppé dans son développement.***

En prenant le teme maladie dans le sens le ***"mal a dit",*** en tenant compte de l'individu dans sa globalité physio-psycho-esprit et de son environnement sociétal, pour la naturopathie, le symtôme **n'est qu'un signe réactionnel de notre organisme de son mal-être.**

## 1) Du silence à l'explosion de la maladie

Quel(le)s malades ont **pris conscience du début** d'un eczéma, d'un ulcère à l'estomac,d'une sclérose en plaque,d'une tumeur.....**au moment précis** lorsque la maladie naît et commenece à se développer?
**Auncun(e)!** Car lorsque la maladie **s'installe** à ses débuts, nous nous rendons compte de rien. **C'est imperceptible.** Ce n'est que plus tard lorsque **le symtôme apparait** que nous **prenons conscience que nous sommes malades.**

>>*"La maladie, c'est un drama en 2 actes, dont le premier se joue dans ***le norme silence de nos tissus toutes lumières éteintes.*** *Quand la douleur ou d'autres gênes arrivent, on est presque ***toujours au second acte.***"

## 2) Petit rappel
Revenons au schéma physiologique, montrant l'apport des éléments nutritifs aux cellules ainsi que l'évacuation des déchets dus au métabolisme cellulaire. Qui se trouve dans la deuxième partie. 

Les cellules étant les plus petites unités structurales vivantes de notre organisme sont interdépendantes les unes des autres. Par ce fait nous pouvons dire que nous sommes physiquement qu'**un ensemble de cellules baignant dans un milieu aqueux**, dans un océan intérieur, possédant **des propriétés vitale assurant**, dans un esprit collectif, **l'unité interne de tout notre organisme.**

La cellule est le **siège de manifestations énergétiques.** Elle respire, digère, élimine, se multiplie, elle est **sensible aux influences du milieu ambiant, extérieur et intérieur à elle.**

>>***C'est de ce milieu ambiant extérieur et intérieur que nos cellules tireront leur vitalité ou leur décrépitude.***

Ce milieu ambiant est nommé en Naturopathie:

- **Les sérums extracellulaires, 15 à 20% du poids du corps.** (sur le schéma, extérieur à la cellule)
- **Le sérum intracellulaire qui représente 50% du poids du corps,** dans lequel se maintiennent les organites cellulaires et **se fait le métabolisme cellulaire.**

*"Nous sommes une baignoire ambulante à remous".*

>>**Conclusion:** C'est dans ce milieu des sérums extra cellulaires que les cellules **prennent les substances,** déversées par le sang à travers la paroi des capillaires sanguins, **dont elles ont besoin et qui leur sont nécessaire** à leur cycle de vie et à leur reproduction.Elles en **transforment une partie** en matière vivante et **accumule une autre** sous forme de réserve.

Sous forme **de déchets elles évacuent se qui est inutilisable.**

>>Il en résulte: ***Si elles absorbent plus de ce qu'elles ont besoin en dépassant les excrétions*** (déchets cellulairs), ***le corps accumule des réserves. Ces réserves sont dépensées lorsque les excrétions sont plus importantes que les absorptions.***

## 3) Comment l'organisme devient malade, comment les maladies se constituent.
>>***Important et à retenir:*** *Chaque cellule trouve ce dont elle a besoin* ***dans le milieu extracellulaire*** *dans lequel elle baigne,* ***de l'importance de la valeur de celui-ci***, *de ce qu'elle* ***va absorber*** *et de ce qu'elle* ***va évacuer.***

Les éléments qui vont se trouver dans le milieu extra cellulaire (sérum extracellulaire), dépendront:

- **De l'état du métabolisme digestif** à la suite des nourritures (solide, liquide, gazeuse -l'air que nous respirons-) **que nous absorbons.**
- **De l'état psychologique, émotionnel:** établis par nos formations mentales dues à nos sensations, désirs.... 
- **De notre état d'esprit:** orgueilleux, avide, coléreux, haineux...
- **De l'état environnemental** dans lequel nous vivons: sociétal, milieu écologique.

Ces quatre états **sont interdépendants**

**a) les cellules reçoivent se dont elles ont véritablement besoin.**
En tenant compte:

- Des apports d'aliments (solides, liquides, gazeux) ***spécifiques à l'Etre Humain.***
- D'un état psychologique ***serein et sans tension*** ou très peu et ***rapidement jugulés.***
- D'un état d'esprit sous l'influence de ***la bienveillance et de la compassion.***
- Tout en évoluant dans ***un environnement approprié.***

>>***Dans ces conditions optimums,*** **l'organisme s'autorégule,** ***et par le bon fonctionnement des organes appropriés***(qui sont appelés en naturopathie: **émonctoires: intestins+foie et vésicule biliaire, reins, poumons et peau)**,***il évacue hors de lui les déchets du métabolisme digestif et cellulaire.***
>>**Nous nous trouvons avec une santé naturelle optimale.**

**b) Les cellules ne reçoivent pas convenablement ce qu'elles ont véritablement besoin et elles doivent aussi faire face à des éléments toxiques.**

- En absorbant **une nourriture non spécifique à l'Etre Humain,** c'est-à-dire **les faux aliments** et **ceux dénaturés** par différentes manipulations. Ces ***faux aliments et ceux dénaturés vont produire une masse de déchets.***
- En nous trouvant dans **des situations psychologiques stressantes, aliénantes...**(peur, soucis, bruits, agitations, insomnies,etc...), qui vont nuire au bon fonctionnement du diencéphale, créant ainsi **des troubles du métabolisme digestif et cellulaire** (digestion difficile, assimilation cellulaire troublée) et **les fonctions d'éliminations complètement perturbées.**
- En ayant **un état d'esprit** sous l'influence de ***la stupidité (illusion),*** de ***la cupidité (convoitise),*** de ***la malveillance (commérage,colère, haine).*** 
- En se trouvant dans **un milieu sociétal** (travail, manipulations mentales...) et **environnemental non appropriés.** Milieu environnemental dans leuel sont déversées ***des particules inadéquates à notre existence*** (produits chimiques, nanoparticules,ondes electromagnétiques, radioactivité....).

Tous ces phénomènes **vont provoquer une surcharge, un encrassement toxémique qui va s'installer dans nos humeurs**, c'est-à-dire dans les liquides humoraux: ***sang, lymphe, sérums extra et intracellulaire*** (voir schéma) qui constituent avec les glandes endocrines (dans lesquelles se situe la ***Force Vitale***) et les centres nerveux, **le terrain organique.**

**c) Réaction de l'organisme.**
Face à cette surcharge, à cet encrassement toxémique, résultant d'une vie "contre nature", l'organisme va réagir suivant des réactions spécifiques en rapport **avec le degré de surcharge toxémique.**

- **Première réaction.**
Lorsque **le seuil de tolérance est atteint,** l'organisme signale **le risque de débordement** de celui-là, par un **appel à l'attention** (fatigue, bouche pâteuse, lourdeur digestive...).
- **Deuxième réaction.**
Lorsque le premier signal lancé par l'organisme n'est pas écouté. Qu'aucune aide lui est apporté pour **éviter le débordement du seuil de tolérance, et que celui-ci est débordé** en continuant une vie "antibiologique". Notre organisme se retrouve avec un taux important d'encrassement humoral.
Par ce fait, il **se manifeste par des tempêtes neuro-hormonales.**
- **Troisième réaction.**
Réprimant violement, abusivement les réations d'autodéfense de l'organisme, la source toxinique **reste active**, entretenue par notre comportement de vie.
L'organisme **se trouve dans l'impuissance** de se manifester pour limiter et neutraliser l'encrassement.

**d) Conclusion.**
Cette progression des substances nocives dans l'organisme, dû à notre façon de vivre, c'est ce qui est nommé  naturopathie: **L'ENCRASSEMENT HUMORAL.** Qui est pour la science naturopathique **la source unique des maladies.** Par ce fait, **la MALADIE EST UNE** ***s'exprimant sous différentes formes (symptômes)*** qui ne sont que des réactions de défense face à l'encrassement humoral.

Donc, **ce n'est pas l'organe malade qu'il faut mettre en cause, mais l'encrassement humoral, c'est-à-dire le TERRAIN.** ***L'organe malade n'est que la conséquence de cet encrassement.***

>> *Claude Bernard, médecin-physiologiste (1813-1878) créateur du concept de milieu intérieur en biologie, que l'on nomme homéostasie, disait:* ***"Le microbe n'est rien, c'est le terrain qui est tout."***

>>*Le docteur Alexandre Salmanoff, diplômé des facultés de Moscou, Berlin et Pavie (1875-1964):**"La vie (organique) est un mouvement continuel de liquide entre les cellules et dans les cellules. L'arrêt de ce mouvement, c'est la mort. Le ralentissement partiel de ce mouvement des liquides dans quelque organe provoque un trouble partiel. Le ralentissement général des liquides extracellulaires et intracellulaires provoque une maladie générale." 

                        

Par la suite nous verrons:

- **L'origine de l'encrassement humoral**
- **Les réactions "neuro-hormonales"**
- **Comment aider l'organisme à s'épurer de cet encrassement humoral**
-  **Le véritable rôle de la Naturopathie Contemporaine.**
